﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Zation
{
	public partial class ListWork : ContentPage
	{
        public List<Tarefa> Tarefa { get; set; }

		public ListWork (List<Tarefa> tarefas)
		{
            BindingContext = this;
            Tarefa = tarefas;
			InitializeComponent ();
		}
	}
}