﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Zation
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddWork : ContentPage
	{
        public List<Tarefa> Tarefas { get; set; }
		public AddWork (List<Tarefa> tarefas)
		{
            Tarefas = tarefas;
			InitializeComponent ();
		}

        public void Salva(Object sender, EventArgs e)
        {
            String Sub = Subjects.Text;
            String Descri = Desc.Text;
            Tarefa tarefas = new Tarefa(Sub, Descri);
            Tarefas.Add(tarefas);

        }
	}
}