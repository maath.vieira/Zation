﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Zation
{
	public class HomeTabbedPage : TabbedPage
	{
		public HomeTabbedPage ()
		{
            List<Tarefa> tarefas = new List<Tarefa>();
            this.Children.Add(new AddWork(tarefas));
            this.Children.Add(new ListWork(tarefas));
        }
	}
}