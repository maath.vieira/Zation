﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zation
{
    public class Tarefa
    {

        public string Subjects { get; set; }
        public string Desc { get; set; }

        public Tarefa(String desc, String sub)
        {
            this.Desc = desc;
            this.Subjects = sub;
        }
    }
}
