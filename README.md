<h1>Xamarin</h1>  
![Xamarin](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Xamarin-logo.svg/1200px-Xamarin-logo.svg.png)  

<h2>Resumo</h2>  
<p>
Xamarin � uma plataforma de desenvolvimento para dispositivos m�veis, podendo trabalhar com iOS, Android e Windows Phone usando a reutiliza��o de c�digo e desenvolvendo um aplicativo para os 3 sistemas operacionais mais utiliados no mercado. 
Os criadores foram Nat Friedman, Miguel de Icaza e Joseph Hill na Colif�rnia. 
Foi adquirida pela Microsoft em 2016. 
</p>  
<p>
Hoje mais de 15 mil empresas usam o xamarin, empresas bem conhecidas como Bosh, Slack e Kellogg's.
</p>

-------

<h2>Requisitos M�nimos</h2>  
<ul>
	<li>Windows 7</li>  
	<li>35GB HDD</li>  
	<li>Visual Studio 2013 ou superior</li>  
	<li>Android SDK</li>  
</ul>

-------

<h2>Instala��o</h2>

-[Android SDK](https://developer.android.com/studio/index.html?hl=pt-br)  
-[Xamarin](https://www.xamarin.com/download)  
<p>Necess�rio fazer cadastro!</p>  
<p>O gerenciador ir� baixar o Visual Studio 2017 Community tamb�m.</p>  

-------

<h2>Principais passos para a instala��o</h2>

<p>Fazer cadastro na p�gina do xamarin.</p>
<p>Executar o gerenciador de 1MB que ser� baixado "visualstudioinstaller.exe".</p>
<p>Selecionar frameworks que voc� deseja baixar, no caso Xamarin.</p>
<p>Espere infinitamente at� sua internet de 2MB fazer o download de 35GB.</p>

-------

<h2>Documenta��o Utilizada</h2>  

-[Xamarin](https://developer.xamarin.com/guides/cross-platform/getting_started/)  

-------

<h2>Vantagens de usar o Xamarin</h2>

<ul>
	<li>Desenvolvimento de aplicativos Nativos.</li>
	<li>Acessar recursos pr�prios de cada S.O. utilizando o C#.</li>
	<li>C�digos menores e desenvolvimento limpo.</li>
	<li>Possibilidade de usar outras frameworks como .NET no projeto.</li>
</ul>

--------

<h2>Desevantagens de usar o Xamarin</h2>

<ul>
	<li>Documenta��o n�o � completamente traduzida.</li>
	<li>Muito "pesado".</li>
</ul>

---------

<h2>V�deo aula</h2>

-[V�deo Aula 01](https://www.youtube.com/watch?v=IF_S-IlY91k)
